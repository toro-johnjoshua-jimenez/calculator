package io.toro.ojtbe.jimenez.calculator.calculator.models;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public final class ExpressionModel {

    @NotNull
    private final String expression;

    @NotNull
    @Min(value = 0)
    private final int decimalPrecision;

    public ExpressionModel(String expression, int decimalPrecision){
        this.expression = expression;
        this.decimalPrecision = decimalPrecision;
    }
}

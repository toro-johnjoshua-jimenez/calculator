package io.toro.ojtbe.jimenez.calculator.calculator.modules;

import io.toro.ojtbe.jimenez.calculator.calculator.models.ExpressionModel;

public interface ExpressionEvaluator {
    public ExpressionModel evaluate(ExpressionModel model);
}

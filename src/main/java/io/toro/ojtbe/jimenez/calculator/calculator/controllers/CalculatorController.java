package io.toro.ojtbe.jimenez.calculator.calculator.controllers;

import io.toro.ojtbe.jimenez.calculator.calculator.errors.ValidationError;
import io.toro.ojtbe.jimenez.calculator.calculator.models.ExpressionModel;
import io.toro.ojtbe.jimenez.calculator.calculator.modules.UdoExpressionEvaluator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
final class CalculatorController {

    @PostMapping("/evaluate")
    ResponseEntity evaluatePostData(@Valid @RequestBody ExpressionModel model){
       ExpressionModel result = UdoExpressionEvaluator.getInstance().evaluate(model);

       return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    ResponseEntity validationError(MethodArgumentNotValidException e){
        BindingResult result = e.getBindingResult();
        ValidationError errorModel = new ValidationError(result.getFieldErrors(), "VALIDATION_ERROR");

        return new ResponseEntity<>(errorModel, HttpStatus.BAD_REQUEST);
    }
}

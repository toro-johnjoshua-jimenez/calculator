package io.toro.ojtbe.jimenez.calculator.calculator.modules;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.udojava.evalex.Expression;
import io.toro.ojtbe.jimenez.calculator.calculator.models.ExpressionModel;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public final class UdoExpressionEvaluator implements ExpressionEvaluator{
    private static final UdoExpressionEvaluator instance = new UdoExpressionEvaluator();

    public ExpressionModel evaluate(ExpressionModel model) {
        Expression expression = new Expression(model.getExpression());
        int decimalPrecision = model.getDecimalPrecision();
        expression.setPrecision(decimalPrecision);
        BigDecimal answer = expression.eval();

        return new ExpressionModel(answer.toString(), decimalPrecision);
    }

    public static ExpressionEvaluator getInstance(){
        return instance;
    }
}

package io.toro.ojtbe.jimenez.calculator.calculator.errors;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public final class ValidationError {
    private final String message;
    private final List<String> causes;

    public ValidationError(List<FieldError> errors, String message){
        this.message = message;
        this.causes = new ArrayList<>();
        parseErrors(errors);
    }

    private void parseErrors(List<FieldError> errors){
        for(FieldError error: errors){
            causes.add("Field name " + error.getField() + " should be " + error.getCode());
        }
    }
}
